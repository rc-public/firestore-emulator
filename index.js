const Firestore = require('@google-cloud/firestore')
const grpc = require('@grpc/grpc-js')

const firestore = new Firestore({ projectId: 'project-test' })

async function main () {
  // Pega a referencia da collection
  const customers = firestore.collection('customers')

  // Adiciona um documento na collection
  const newCustomer = await customers.add({ name: 'customer1', code: 55012, address: 'Rua Tal, 88'})
  console.log(`Customer gerado com id ${newCustomer.id}\n\n`)
  
  console.log('vamos ver como isso ficou na base')
  const savedCustomer = await customers.doc(newCustomer.id).get()
  console.log(savedCustomer.data())
  console.log('\n\n')

  console.log('agora os customers que tem na collection são:')
  const allQuery = await customers.get()
  let count = 0
  allQuery.forEach(doc => {
    count++
    console.log(doc.ref.path) 
  })
  console.log(`${count} registros\n\n`)

  const idToDelete = newCustomer.id
  console.log('Agora que tal deletar?!')
  console.log(`primeiro buscamos o customer pelo ID ${idToDelete}`)
  const foundCustomer = await customers.doc(newCustomer.id)
  console.log('agora a gente deleta!')
  await foundCustomer.delete()
  console.log('Pronto! \n\n')

  console.log('agora os customers que tem na collection são:')
  const allQuery2 = await customers.get()
  let count2 = 0
  allQuery2.forEach(doc => {
    count2++
    console.log(doc.ref.path) 
  })
  console.log(`${count2} registros\n\n`)
}

main()